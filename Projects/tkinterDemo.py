import tkinter as tk

def on_button_click():
    label.config(text="EchaMama, Tkinter!")

# Create the main window
root = tk.Tk()
root.title("Tkinter Example")

# Create a label widget
label = tk.Label(root, text="Welcome to Tkinter")
label.pack(padx=20, pady=20)

# Create a button widget
button = tk.Button(root, text="Click Me", command=on_button_click)
button.pack(padx=10, pady=10)

# Start the GUI event loop
root.mainloop()
