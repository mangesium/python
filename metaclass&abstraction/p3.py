class CWMeta(type):
    def __new__(cls,clsname,basecls,clsdict):
        print(clsname)
        print("In new CWMeta")
        print(clsdict)
        return super().__new__(cls,clsname,basecls,clsdict)
    def __init__(self,clsname,basecls,clsdict):
        print("IN CWMeta constructor")
    pass
class Child(metaclass=CWMeta):
    print("In Child")
    def __new__(cls):
        print("In Child's new ")
        return super().__new__(cls)
    x=10
    y=20
    def gun(self):
        print("In gun")
class xyz(Child):
    print("In XYZ")
    a=300
    b=400
    pass
obj=xyz()
print(type(Child))
#obj2=Child()
#obj2=Child("Child",(CWMeta,),{'a':10,'b':20})
#print(obj.x)