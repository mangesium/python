from abc import ABC, ABCMeta,abstractmethod

class Parent(ABC):
    @abstractmethod
    def show(self):
        pass
    @abstractmethod
    def info(self):
        pass
#obj=Parent()
class Child(Parent):
    
    def show(self):
        print("In Show")
    def info(self):
        print("In info")
class xyz:
    pass
    
obj=Child()
obj.show()
obj.info()

print(type(ABCMeta))
print(type(Parent))
print(type(Child))
print(type(xyz))