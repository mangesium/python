
#<-------Difference betwn property method and normal instance method------->#

class Employee():
    def __init__(self,ename,sal):
        self.ename=ename
        self.sal=sal
    @property
    def getinfo(self):
        return self.ename,self.sal
    @getinfo.setter
    def getinfo(self,ename):
        self.ename=ename

    def notpropertymethod(self):
        return self.ename,self.sal

obj=Employee("Kanha",50)
print(obj.getinfo)
print(obj.notpropertymethod)
obj.getinfo="Pitya bhai"
print(obj.getinfo)

