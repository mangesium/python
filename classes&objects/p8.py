class A:
    count=60
    def myclassmethod(fun):
        def inner(*args):
            fun(args[0].__class__)
        return inner
    def __init__(self,name,no):
        self.name=name
        self.no=no
    @myclassmethod
    def display(cls):
        print(cls)
        cls.count=59

class B:
    def mystaticmethod(fun):
        def inner(args):
            fun()
        return inner
    def __init__(self,name,no):
        self.name=name
        self.no=no

    @mystaticmethod
    def printdata():
       pass
        
obj1=A("Mangesh",32)
obj1.display()
print(obj1)
obj2=B("manya",37)
obj2.printdata()