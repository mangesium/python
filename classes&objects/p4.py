class Employee:

    cname="MSquare"

    def __init__(self):
        self.ename="Mangesh"
        self.empId=369
    
    def display(self):
        print(self.ename)
        print(self.empId)

obj1=Employee()
print(obj1.cname)
Employee.cname="MCapital"
print(obj1.cname)

obj2=Employee()
print(obj2.cname)

obj3=tuple()
print(obj3.__repr__)
