
class Addition:
    def mydispatch(fun,clt=None,clt2=None,clt3=None):
        print(fun,clt,clt2,clt3)
        def inner(*type):
            return type[0]
        return inner
    
    @mydispatch(int,int)
    def add(self,x,y):
        return x+y 
    @mydispatch(int,int,int)
    def add(self,x,y,z):
        return x+y+z
    @mydispatch(float,float)
    def add(self,x,y):
        return x+y
    
    @mydispatch(float,float,float)
    def add(self,x,y,z):
        return x+y+z

obj=Addition() 
print(obj.add(10,20,30))