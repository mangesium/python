import array

arr1=array.array('i',[10,20,30])
arr2=arr1        #Shallow copy
print(id(arr1))
print(id(arr2))

arr2=array.array('i',[ele for ele in arr1])    #Deep copy
print(id(arr1))
print(id(arr2))