class Parent:
    def __init__(self):
        print("Parent Constructor")
        self.x=10
        self.__z=1000
        
    def disp(self):
        print(self.x)
        print(self.__z)     #Private Variable

class Child(Parent):
    def __init__(self):
        super().__init__()          #Parent construction initialization
        print("Child Constructor")
        self.y=10
    def show(self):
        print(self.x)
        print(self.y)
        print(self._Parent__z)  #Accessing Private Variable in child class

obj=Child()
# obj.disp()
obj.show()
print(dir(obj))