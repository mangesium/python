def concat(fun):
    def inner(*args):
        ret=fun(*args)
        if(ret[2]=="false"):
            return args[0],args[1]
        return args[0]+args[1]
    return inner

def anagram(fun):
    def inner(*args):
        ans=fun(*args)
        str1=sorted(args[0].lower())
        str2=sorted(args[1].lower())
        if(str1==str2):
            return args[0],args[1],"true"
        return args[0],args[1],"false"
    return inner
@concat
@anagram
def printstr(str1,str2):
    return str1,str2

# print("Mangesh","Manjare")
print(printstr("Mangesh","Manjare"))
print(printstr("Mangesh","Mangesh"))