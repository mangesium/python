import array

arr1 = array.array('i',[1,2,3,4,5])
arr2=arr1   #only assigning another refrence to an object(shallow copy)
arr3 = array.array(arr1.typecode,[i for i in arr1]) #creating new array(deep copy)
# arr3 = array.array(arr1.typecode,[arr1[0:5]]) #Error
print(arr1)
print(arr2)
print(arr3)

arr1[0]=10
print()
print(arr1)
print(arr2)
print(arr3)

arr4 = array.array('I',[-11,2,3,4,5])   #error:OverflowError: can't convert negative value to unsigned int