class cric:
    format="T20"

    def __init__(self,name,jerNo):
        self.name=name
        self.jerNo=jerNo

    @classmethod
    def displayData(cls):
        print("In class method")
        print(cls)
        print(cls.format)
    
    def display(self):
        print("In instance method")
        print(self.name)
        print(self.jerNo)

    @staticmethod
    def auction():
        print("In static method")
        
    
player1=cric("Dhoni",7)
player2=cric("Virat",18)
print(player1)

player1.displayData()
player1.display()
player1.auction()
print(dir(player1))