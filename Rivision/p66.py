import numpy

def printarr(arr,num):
    if(num>=0):
        printarr(arr,num-1)
        print(arr[num])

num = int(input("Enter size of array : "))
arr=numpy.zeros(num,int)
for i in range(num):
    arr[i]=int(input("Enter element : "))
printarr(arr,num-1)
