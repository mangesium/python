def ispalindrome(fun):

    def inner(args):
        ans = fun(args)
        a=0
        num=ans
        while(ans!=0):
            a=a*10+ans%10
            ans//=10
        if(a==num):
            return True
        return False
    return inner

@ispalindrome
def getdetails(args):
    return args

print(getdetails(11))