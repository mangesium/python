#returning function from a function

def Outer():

    def Inner():
        print("Inner Function")
    
    print("Outer function")
    return Inner
retfun = Outer()
print(retfun())