class X:
    x=10
    def __init__(self) -> None:
        super().__init__()
        print("X constructor")

class Y:
    x=20
    def __init__(self) -> None:
        super().__init__()
        print("Y constructor")

class A(X):
    x=30
    def __init__(self) -> None:
        super().__init__()
        print("A constructor")

class B(X):
    x=40
    def __init__(self) -> None:
        super().__init__()
        print("B constructor")
class C(Y):
    x=50
    def __init__(self) -> None:
        super().__init__()
        print("C constructor")
class Child(C,B,A):
    def __init__(self) -> None:
        super().__init__()
        print("Child constructor")
        

obj = Child()
print(obj.x)

    