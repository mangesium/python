python3 -m dis p31.py > out.txt     // To store bytecode in .txt file
python3 -m py_compile Program.py    // To compile python code
https://scikit-learn.org/stable/    // Library for machine learning
https://www.python.org/             // python
https://www.tensorflow.org/         // Library for machine learning