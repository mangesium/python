#Method local inner class 
class Outer:
    def __init__(self):
        print("Outer constructor")
        self.x=10
    def dispData(self):
        print(self.x)
        class Inner:
            y=20
            def __init__(self):
                print("Inner constructor")
                self.inn=30

            def dispInner(self):
                print(self.inn)
                print(self.y)
        
        z=40
        print(z)
        innobj=Inner()
        innobj.dispInner()
        print(innobj)
        return innobj

out=Outer()
inn = out.dispData()
print(inn)
inn.dispInner()

