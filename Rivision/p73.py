def outerFunction():
  #x is a local variable to outerFunction()    
  x =5
  print("value of 'x' in outerFunction=",x)
  def innerFunction():
    #x is nonlocal to innerFunction()
    nonlocal x
    x=6
    print("value of 'x' in innerFunction=",x)
  innerFunction() 


outerFunction()