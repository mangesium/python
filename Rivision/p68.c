#include <stdio.h>
#include <string.h>

void main()
{
    char str[] = "Mangesh";
    printf("%s\n", str);

    for (int i = 0; i < strlen(str) / 2; i++)
    {
        char temp = str[i];
        str[i] = str[strlen(str) - i - 1];
        str[strlen(str) - i - 1] = temp;
    }
    printf("%s\n", str);
}