#include <stdio.h>
void main()
{
    int x = 3;
    int y = -10;
    printf("%d\n", x && y); // false if one of them is zero
    printf("%d\n", x || y); // true if one of them is non-zero

    // printf("%d\n", ++x + ++x);
    // printf("%d\n", ++x + x++);
    printf("%d\n",++x + ++x + ++x);
}