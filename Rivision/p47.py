s= (2,5,8)
# Python conversion for list and tuple to one another
x = list(s)
print(x)
print(type(x))

# Add items by using insert ()
x.insert(1,100)
x.append(500)
print(x)

# Use tuple to convert a list to a tuple ().
s_insert = tuple(x)
print(s_insert)
print(type(s_insert))