#calling outer class variable from inner class's instance method
class Outer:
    x=10
    def __init__(self):
        print("Outer constructor")
        self.out=10

    class Inner:
        y=20
        def __init__(self,out):
            print("Inner constructor")
            self.inn=20
            self.out=out

        def dispInner(self):
            print(self.inn)
            print(self.y)
            self.out.dispOuter()
    
    @classmethod
    def dispOuter(self):
        print(self.x)

out=Outer()
# out.dispOuter()
inn=Outer.Inner(out)
inn.dispInner()