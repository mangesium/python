#Normal Inner class
class Outer:
    x=10
    def __init__(self):
        print("Outer constructor")
        self.out=10
        self.Inner = self.Inner()

    class Inner:
        y=20
        def __init__(self):
            print("Inner constructor")
            self.inn=20
        def dispInner(self):
            print(self.inn)
            print(self.y)
    
    def dispOuter(self):
        print(self.out)
        print(self.x)

out=Outer()