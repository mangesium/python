def reverse(fun):
    print("In Reverse")
    def inner(*args):
        print("In Inner Reverse")
        str1=fun(*args)
        return str1[-1::-1]
    print("ret Inner Reverse")
    return inner
def concat(fun):
    print("In Concat")
    def inner(*args):
        print("In Inner Concat")
        ret=fun(*args)
        str2=""
        for x in ret:
            str2=str2+x
        return str2
    print("ret Inner Concat")
    return inner

@reverse
@concat
def printstr(str1,str2):
    print("In PrintStr")
    return str1,str2

# print("Mangesh","Manjare")
print(printstr("Mangesh","Manjare"))