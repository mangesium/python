import numpy
arr1=numpy.array([9,8,7,6,5,4,3,2,1])

#view
arr2=arr1.view()    
print(id(arr1)) 
print(id(arr2))

arr2[0]=10
print(arr1)
print(arr2)

#copy
arr3 = arr1.copy()
print(id(arr1)) 
print(id(arr3))

arr3[0]=11
print(arr1)
print(arr3)