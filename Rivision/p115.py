class Parent1:
    def __init__(self) -> None:
        self.x=10
        print("Prent1 constructor")
        super().__init__()

class Parent2:
    def __init__(self) -> None:
        self.y=20
        print("Prent2 constructor")
        super().__init__()

class Child(Parent1,Parent2,):
    def __init__(self) -> None:
        self.z=30
        print("Child constructor")
        super().__init__()
        

obj = Child()
print(obj.x)
print(obj.y)
print(obj.z)