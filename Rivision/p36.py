import numpy

arr = numpy.array([10,9,8,7,6,5,4,3,2,1],int)
print(arr)
#where
arr2 =numpy.where(arr%2==0,arr,0)
print(arr2)

#argsort
arr3 = numpy.argsort(arr)   #sorted index's of elements will get stored in following array
print(arr3)

#sort
arr4=numpy.sort(arr)
print(arr4)