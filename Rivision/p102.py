class cric:
    format="T20"

    def __init__(self,name,jerNo):
        print("In constructor")
        self.name=name
        self.jerNo=jerNo

    @classmethod
    def displayData(cls):
        print(cls.format)

    def fun(self):
        print(self.name)
        print(self.jerNo)

def fun():
    pass

player1=cric("Dhoni",7)
print(type(player1))    #<class '__main__.cric'>
print(type(player1.displayData))    #<class 'method'>
print(type(player1.fun))    #<class 'method'>
print(type(fun))    #<class 'function'>
player1.displayData()