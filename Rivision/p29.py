import array
arr =array.array('i',[1,2,3])
print(arr)

#append
arr.append(4)
arr.append(1)
arr.append(5)
arr.append(6)
arr.append(7)
arr.append(99)
print(arr)
print(len(arr))

#count
print("count 1 = ",arr.count(1))
print("count 2 = ",arr.count(2))
print("count 3 = ",arr.count(3))

#index
print(arr.index(99))
# print(arr.index(0))   #error

#insert
arr.insert(0,10000)
print(arr)

#pop
arr.pop()
print(arr)

#remove
arr.remove(1)
print(arr)

#reverse
arr.reverse()
print(arr)

#tolist
lst = arr.tolist()
print(arr)
print(lst)

#fromlist

arr.fromlist(lst)
print(arr)

#itemsize
print(arr.itemsize)
carr =array.array('u',['a','b','c'])
print(carr.itemsize)    #4 is the output but due to vscode's config. it shows 2 bytes

#buffer_info
print(len(arr))
print(arr.buffer_info())
print(carr.buffer_info())


