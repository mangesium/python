import numpy

#array
arr = numpy.array([1,2,3,4,"mangesh"])
print(arr)
print(type(arr))

#linspace
arr1=numpy.linspace(0,1,10)   #makes the number of elements betweenn the first and second range
print(arr1)
print(type(arr1))

#arange
arr2 =numpy.arange(10,20)
print(arr2)
print(type(arr2))

#zeroes
arr3 =numpy.zeros(10,int)
print(arr3)
print(type(arr3))

#ones
arr4 =numpy.ones(10,int)
print(arr4)
print(type(arr4))