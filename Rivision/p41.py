import numpy

arr1 = numpy.array([1,2,3,4])
arr2 = numpy.array([[1,2,3,4],[5,6,7,8],[1,2,3,4]])

#ndim
print(arr1.ndim)
print(arr2.ndim)
#size
print(arr1.size)
print(arr2.size)
#itemsize
print(arr1.itemsize)
print(arr2.itemsize)
#nbytes
print(arr1.nbytes)
print(arr2.nbytes)
#shape
print(arr1.shape)
print(arr2.shape)
#dtype
print(arr1.dtype)
print(arr2.dtype)

