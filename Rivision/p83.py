def mult(fun):

    def magic(x):
        ans=fun(x)
        return ans*x
    return magic

@mult
def sqr(x):
    return x*x

print(sqr(5))