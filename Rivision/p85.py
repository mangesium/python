import numpy
def sortarr(fun):

    def magic(x):
        ans=fun(x)
        return numpy.sort(ans)
    return magic

@sortarr
def fun(x):
    return x

arr=numpy.array([5,4,3,2,1],int)
print(arr)
print(fun(arr))
