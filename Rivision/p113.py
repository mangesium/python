class Parent1:
    def __init__(self) -> None:
        super().__init__()
        print("Prent1 constructor")

class Parent2:
    def __init__(self) -> None:
        super().__init__()
        print("Prent2 constructor")
class Parent3:
    def __init__(self) -> None:
        super().__init__()
        print("Prent3 constructor")
class Child(Parent1,Parent2,Parent3):
    def __init__(self) -> None:
        super().__init__()
        print("Child constructor")
        

obj = Child()
# print(Child.mro())

    