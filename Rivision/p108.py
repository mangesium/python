class Parent:
    def __init__(self):
        print("Parent Constructor")
        self.x=10
    def disp(self):
        print(self.x)

class Child(Parent):
    def __init__(self):
        print("Child Constructor")
        self.y=10
    def show(self):
        # print(self.x)
        print(self.y)

obj=Child()
obj.disp()  #AttributeError: 'Child' object has no attribute 'x'
# obj.show()