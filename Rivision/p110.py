class Parent:
    x=10
    def __init__(self):
        print("Parent Constructor")
        self.y=20

    @classmethod
    def show(cls):
        print(cls.x)
    
    def disp(self):
        print(self.x)
        print(self.y)
class Child(Parent):
    def __init__(self):
        print("Child Constructor")
        super().__init__()
    
    def childdisp(self):
        print(self.x)

obj=Child()
obj.childdisp()
obj.show()
obj.disp()