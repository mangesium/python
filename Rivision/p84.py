def reverse(fun):

    def magic(x):
        ans=fun(x)
        return ans[-1::-1]
    return magic

@reverse
def printstr(x):
    return x

print(printstr("Mangesh"))