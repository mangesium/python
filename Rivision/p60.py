#variable number of keyword arguments

def employee(**emp):
    print(emp)
    print(type(emp))

employee(employee="karan",sal=15.97,age=23,type="chutya")
# employee(employee="karan",sal=15.97,age=23,type="chutya",employee="ganesh") #SyntaxError: keyword argument repeated: employee