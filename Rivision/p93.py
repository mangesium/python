class Student:
    def __init__(self,name,id):
        print("In Constructor1")
        self.name=name
        self.id=id
    
    def __init__(self,company):
        print("In Constructor2")
        self.company="M Square"
    
    def display(self):
        print(self.name)
        print(self.id)

obj = Student("Mangesh")
obj=Student("mangesh",50)   #TypeError: Student.__init__() takes 2 positional arguments but 3 were given
