class Parent:
    x=10
    def __init__(self):
        print("Parent Constructor")
        self.y=20
    
    @classmethod
    def show(cls):
        print(cls.x)
    
    def disp(self):
        print(self.x)
        print(self.y)

class Child1(Parent):
    x=110
    def __init__(self):
        super().__init__()
        self.y=30
        print("Child1 Constructor")
    @classmethod
    def show(cls):
        print(cls.x)
    
    def ChildDisp(self):
        print(super().x)
        print(self.x)

class Child2(Child1):
    def __init__(self):
        super().__init__()
        print("Child2 Constructor")
    
    def ChildDisp(self):
        print(super().x)
        print(self.x)     
obj=Child2()
obj.show()
obj.disp()

