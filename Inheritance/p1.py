class Westeros:
    Kingseat="Iron Throne"
    Kingdoms=7
    def __init__(self,ThroneHair):
        print("The King of the Andals,the Rhoynar,and the First Men sat,located in the Great Hall of the Red Keep in the city of King's Landing Welcomes you to Westeros.")
        self.ThroneHair=ThroneHair
    def Battle(self):
        print(self.ThroneHair,"must know",self.Kingdoms,"Kingdoms are allies of the throne")        

    @classmethod
    def PowerandControl(cls):
        print("The one who sits on ",cls.Kingseat,"will be the Lord of the Seven Kingdoms")

class Stark(Westeros):
    def __init__(self,name,House):
        super().__init__("Bran Stark")
        print("Winter is coming")
        self.name=name
        self.House=House
    
    def Nightswatch(self):
        print(self.name,"of house",self.House,"Here my words and witness to my vow night gathers and my watch begins it shall not end until my death")

obj=Stark("John snow","Stark")
'''
print(id(Westeros.Kingseat))
print(id(obj.Kingseat))
obj.Kingseat="Gold Throne"
print(id(obj.Kingdoms))
obj.Kingdoms="Iron Throne"
print(id(obj.Kingdoms))
'''
obj.PowerandControl()
obj.Battle()
obj.Nightswatch()
