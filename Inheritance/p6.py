class X:
    def __init__(self):
        print(super())
        super().__init__()
        print("X constructo")
class Y:
    def __init__(self):
        print(super())
        super().__init__()
        print("Y constructo")
class Z:
    def __init__(self):
        print(super())
        #super().__init__()
        print("Z constructo")
class A(X):
    def __init__(self):
        print(super())
        super().__init__()
        print("A constructo")
class B(Y):
    def __init__(self):
        print(super())
        super().__init__()
        print("B constructo")
class C(Z):
    def __init__(self):
        print(super())
        #super().__init__()
        print("C constructo")
class Child(A,B,C):
    def __init__(self):
        super().__init__()
        print("Child Constructor")

obj=Child()
print(Child.mro())