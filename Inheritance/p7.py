class A:
    def add(self):
        print("10")
        super().add()

class B:
    def add(self):
        print("20")

class C(A,B):
    def __init__(self) -> None:
        print("In Child")

obj=C()
obj.add()