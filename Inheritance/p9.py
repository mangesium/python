class A:
    def __init__(self):
        print(super()) 
        super().__init__()
        print("A-Constructor")
class B:
    def __init__(self):
        print(super()) 
        super().__init__()
        print("B-Constructor")
class X(A):
    def __init__(self):
        print(super()) 
        super().__init__()
        print("X-Constructor")
class Y(A):
    def __init__(self):
        print(super()) 
        super().__init__()
        print("Y-Constructor")
class Z(A):
    def __init__(self):
        print(super())
        super().__init__()
        print("Z-Constructor")
class P(B):
    def __init__(self):
        print(super())
        super().__init__()
        print("P-Constructor")
class R(Z):
    def __init__(self):
        print(super())
        super().__init__()
        print("R-Constructor")
class Q(X,Y):
    def __init__(self):
        print(super())
        super().__init__()
        print("Q-Constructor")
class M(Q,R,P):
    def __init__(self):
        print(super())
        super().__init__()
        print("M-Constructor")
obj=M()
print(M.mro())