class A:
    def __init__(self):
        super().__init__()
        print("A-Constructor")
class B:
    def __init__(self):
        super().__init__()
        print("B-Constructor")
class X(A):
    def __init__(self):
        super().__init__()
        print("X-Constructor")
class Y(A):
    def __init__(self):
        super().__init__()
        print("Y-Constructor")
class P(B):
    def __init__(self):
        super().__init__()
        print("P-Constructor")
class Q(B):
    def __init__(self):
        super().__init__()
        print("Q-Constructor")
class M(X,Y,P,Q):
    def __init__(self):
        super().__init__()
        print("M-Constructor")
obj=M()
print(M.mro())