class Parent:
    x=10
    
class Child1(Parent):
    #x=110
    pass

class Child2(Child1):
    pass

obj1=Child1()
obj2=Child2()
print(Child1.x)
print(Child2.x)
Child2.x=20
print(Child1.x)
print(Child2.x) 

'''
print(obj1.x)
print(obj2.x)
obj1.x=20
print(obj1.x)
print(obj2.x)
Parent.x=50
obj1.x=50
print(obj1.x)
print(obj2.x)
Parent.x=40
print(obj1.x)
print(obj2.x)
'''