class Demo:
    def __init__(self):
        self.x=10
        self._y=20
        self.__z=30
    def fun(self):
        print(self.x)
        print(self._y)
        print(self.__z)

obj=Demo()
obj.fun()
print(obj.x)
print(obj._y)
print(obj._Demo__z)

print(dir(obj))